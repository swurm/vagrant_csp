## Vagrant VM für das C/S-Praktikum

Das hier enthaltene _Vagrantfile_ erstellt Euch automatisch eine vorkonfigurierte 
virtuelle Maschiene. Mit dieser könnt Ihr die Aufgaben, die Ihr sonst im Labor hätte
machen müssen, auch zu Hause durchfühern.

Die VM ist ein Ubuntu 15.04 Server und wird mit Hilfe der Software _Vagrant_ erstellt, sowie automatisch
konfiguriert. Sie richtet dazu eine _VirtualBox_ VM ein. Darüber hinaus sind auch alle Middleware-
Technologien, sowie alle Tools vorhanden die Ihr so braucht. Also z.B.: _Java SE 6 bis 8_, eine komplett
eingerichtete _MySQL_-Datenbank, _Tomcat7_, _Axis2_, _openEJB_ und _JacOrb_.

Wenn Ihr Euch also ein wenig mit Linux auskennt, sollte das hier eine recht komfortable Lösung für zu Hause 
sein.

### Installation/Konfiguration

Um die VM einfacher zu verteilen wird das Tool Vagrant benutzt. Es läd das von mir konfiguriete Ubuntu-Image
herunter und richtet dann die VM für Virtualbox ein. Damit das funktioniert, müsst Ihr Vagrant installieren.
Es ist für alle großen Betriebssystem verfügbar und kann von https://www.vagrantup.com kostenlos bezogen
werden. Außerdem findet ihr hier noch ein keines Tutorial, das Ihr durchlesen solltet. 
Falls ihr VirtualBox noch nicht installiert habt, könnt ihr es von hier herunterladen:
https://www.virtualbox.org. Windows Users installieren bitte noch OpenSSH-Client von z.B. hier: http://www.mls-software.com/opensshd.html .

Nachdem Ihr sowohl Vagrant, als auch VirtualBox installiert habt, folgt bitte folgenden Schritten um die VM
einzurichten:

1. Öffnet als erste ein Terminal 
2. Klont dieses Git Repo ```$ git clone git@gitlab.com:swurm/vagrant_csp.git ```.
Wenn ihr kein Git installiert habt könnt iher euch oben über die Schaltfläche auch einfach die Files als Zip-Archive herunterladen.
3. Danch mit ```$ cd vagrant_csp``` in das neue Verzeichnis wechseln.
4. In diesem Verzeichnis gebt Ihn nun ```$ vagrant up``` ein. Mit diesem Befehl wird die Einrichtung der VM
gestartet. Dies kann je nach Geschwindigkeit eurer Internetverbindung lange dauern, da das Ubuntu-Image ca.
1,3Gb groß ist.
5. Nachdem Vagrant fertig ist, könnt ihr eine SSH-Sitzung mit der VM durch ```$ vagrant ssh``` herstellen.
6. Als nächstes solltet Ihr die IP der VM ermitteln, diese wird oft im Praktikum gebraucht. Mit 
```$ ifconfig``` werden alle Netztwerkinterfaces angezeigt. Die IP die Ihr sucht, gehört zu Interface _eth1_.
7. Mit ```$ exit`` könnt dir die SSH-Sitzung verlassen und landet wieder in eurem Host-Terminal.

Um die VM auszuschalten genügt ein ```$ vagrant halt```. Mit ```$ vagrant destroy``` entfernt Ihr die VM ohne
Rückstände von Eurem System (Vorsicht es gehen alle Datei/Daten verloren die nicht im Verzeichnis /vagrant
liegen!!!). Das Kommando ist aber nützlich, wenn Ihr etwas an der Middleware verstellt und nicht mehr genau
wisst was :wink:. Ihr könnt dann einfach den Anfangszustand mit einem ```$ vagrant
up```wiederherstellen.

### Anmerkungen

1. Falls ssh mit public key Verfahren nicht funktionieren sollte, könnt Ihr euch 
mit dem __Benutzer:__ vagrant und mit __Password:__ vagrant auch so anmelden 
(z.B. direkt in VirtualBox).

2. Bitte achtet darauf die neuste Version von Vagrant zu installiern und nicht 
z.B. über einen Paketmanager wie _apt_, da hier oft zu alte Versionen hinterlegt sind! 

3. Die Datenbank und der Benutzer sind entgegen des Setups im Labor anders. Die Datenbank der 
VM heißt __csp__ und die Zugangsdaten sind: Benutzername: __csp__ Passwort: __csp2015__

### Tipps RMI

Für die **8LP-Aufgaben**: Tomcat sollte hier schon beim Start der VM laufen. Falls 
dem nicht so ist, einfach ein ```$ sudo service tomcat start``` eintippen. 
Alternativ könnt Ihr auch den Apache2 als Codebase für das entfernte 
Klassen-Laden benutzen. Das jar-Archiv sollte hier im Verzeichnis /var/www/html/jars 
plaziert werden.

### Tipps CORBA

Kleiner Spickzettel für die IDL https://gitlab.com/swurm/vagrant_csp/blob/master/cheatsheets/idl_cheat_sheat.pdf

### Tipps OpenEJB
t.b.a.

### Tipps AXIS2
Benutzername  und Passwort des Tomcat: __admin__

Benutzername AXIS2: __admin__, Passwort __axis2__


Fix für den Compilier-Fehler mit __ant__:
1. Umgebungsvariablen in ```/etc/environment``` ändern. 
    * ```$ sudo nano /etc/environment```
    * Hier nun folgende Zeilen austauschen:
        * aus AXIS_HOME="/opt/axis2-1.6.3" wird __AXIS2_HOME="/opt/axis2-1.6.3"__
        * aus JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64" wird __JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64"__
2. Java zu Version 7 wechseln:
    * ```$ sudo update-alternatives --config java``` dann den Einrag mit JDK 7 auswählen.
    * ```$ sudo update-alternatives --config javac``` dann den Einrag mit JDK 7 auswählen.
    * ```$ sudo update-alternatives --config jar``` dann den Einrag mit JDK 7 auswählen.
3. VM mit ```$ exit``` verlassen und mit ```$ vagrant reload``` neu starten.

