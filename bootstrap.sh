#!/usr/bin/env bash

#MySQL root pw
PASSWORD='pw123'

#Colors for echo command
PURPLE='\033[35m'
CYAN='\033[36m'
NC='\033[0m'

#Create schema csp
mysql --user=root --password=$PASSWORD -e "CREATE SCHEMA csp;"
#Create all tables needed and import data form data.sql
mysql --user=root --password=$PASSWORD  csp </vagrant/res/data.sql
#Configure MySQL for external access
#Copy config file
cp -f /vagrant/res/my.cnf /etc/mysql/my.cnf
#create new db user
mysql --user=root --password=$PASSWORD -e "CREATE USER 'csp'@'%' IDENTIFIED BY 'csp2015';"
#Grant access to the databases
mysql --user=root --password=$PASSWORD -e "GRANT ALL ON csp.* TO 'csp'@'%';"
#Restart MySQL 
service mysql restart
